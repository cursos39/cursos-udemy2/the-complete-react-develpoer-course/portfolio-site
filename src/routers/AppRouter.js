import React from 'react'
import Header from '../components/Header'
import HomePage from '../components/HomePage'
import PortfolioListPage from '../components/PortfolioListPage'
import NotFoundPage from '../components/NotFoundPage'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import PortfolioItem from '../components/PortfolioItemPage'
import Contact from '../components/ContactPage'

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={HomePage} exact={true}/>
        <Route path="/portfolio" component={PortfolioListPage} exact={true}/>
        <Route path="/portfolio/:portfolioId" component={PortfolioItem} />
        <Route path="/contact" component={Contact} exact={true} />
        <Route component={NotFoundPage}/>
      </Switch>
    </div>
  </BrowserRouter>
)

export default AppRouter
